#import "FSFireButton.h"
#import "FSEventBus.h"


/**
 * Extend event object if needed
 */
@interface FSFireButtonSelectedEvent()

- (id)initWithButton:(FSFireButton *)button;

@end

/**
 * Event object implementation
 */
@implementation FSFireButtonSelectedEvent

- (id)initWithButton:(FSFireButton *)button
{
    self = [super init];
    if (self) {
        self.sender = button;
    }
    return self;
}

@end

/** ***************************************************************/

@implementation FSFireButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)clicked:(id)sender
{
    [[[FSFireButtonSelectedEvent alloc] initWithButton:self] fire];
}

@end
