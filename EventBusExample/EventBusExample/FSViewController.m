#import "FSViewController.h"
#import "FSFireButton.h"
#import "FSCustomTextView.h"

@implementation FSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    FSFireButton *button = [FSFireButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Fire" forState:UIControlStateNormal];
    [self.view addSubview:button];
    button.frame = CGRectMake(self.view.center.x - 22.f, 50.0f, 44.0f, 44.0f);
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    FSCustomTextView *textView = [FSCustomTextView new];
    [self.view addSubview:textView];
    textView.frame = CGRectMake(0.0f, 200.0f, 320.0f, 440.0f);
    textView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleRightMargin;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
