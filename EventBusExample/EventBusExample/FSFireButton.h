#import <Foundation/Foundation.h>
#import "FSEvent.h"

@interface FSFireButton : UIButton

@end

DECLARE_EVENT(FSFireButtonSelectedEvent, FSEvent)
/**
 * You can add here properties, with data what need for event
 */
@property (strong) FSFireButton *sender;
DECLARATION_END
