#import "FSCustomTextView.h"
#import "FSFireButton.h"
#import "FSEventBus.h"

@interface FSCustomTextView() <FSFireButtonSelectedEventListener>

@end

@implementation FSCustomTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[FSEventBus instance] subscribe:self];
    }
    return self;
}

- (void)dealloc
{
    [[FSEventBus instance] unsubscribe:self];
}

#pragma mark - FSFireButtonSelectedEventListener

- (void)onFSFireButtonSelectedEvent:(FSFireButtonSelectedEvent *)event
{
    [self setText:[NSString stringWithFormat:@"%@\r\nButton '%@' - clicked", self.text, event.sender.titleLabel.text]];
}

@end
