#import <Foundation/Foundation.h>

@interface FSEvent : NSObject

- (void)fire;

@end

/**
    start event declaration
    @name - event class name
    @parent - parent class name
 */
#define DECLARE_EVENT(name, parent) \
            @class name ; \
            @protocol name##Listener \
            - (void) on##name : ( name *)event; \
            @end \
            @interface name : parent

/**
    ends event declaration
 */
#define DECLARATION_END \
            @end
