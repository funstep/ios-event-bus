#import <Foundation/Foundation.h>

@interface FSEventBus : NSObject

+ (FSEventBus *)instance;

- (void)subscribe:(NSObject *)receiver;
- (void)unsubscribe:(NSObject *)receiver;

- (void)fire:(id)event;

@end