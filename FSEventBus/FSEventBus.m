#import "FSEventBus.h"

@interface FSEventBus ()

@property (nonatomic, retain) NSMutableArray *receivers;

@end

@implementation FSEventBus

- (id)init
{
    self = [super init];
    if (self) {
        self.receivers = [NSMutableArray array];
    }
    return self;
}

- (void)dealloc
{
    self.receivers = nil;
    [super dealloc];
}

- (void)fire:(id)event
{
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@:", NSStringFromClass([event class])]);

    for (NSObject* receiver in self.receivers) {
        if ([receiver respondsToSelector:selector]) {
            [receiver performSelector:selector withObject:event];
        }
    }
}

+ (FSEventBus *)instance
{
    static dispatch_once_t onceToken;
    static FSEventBus *_self = nil;
    dispatch_once(&onceToken, ^() {
        _self = [FSEventBus new];
    });
    return _self;
}

- (oneway void)release
{
    // do nothing
}

- (id)retain
{
    // do nothing
    return self;
}

#pragma mark - public mthods

- (void)subscribe:(NSObject *)receiver
{
    if ([self.receivers containsObject:receiver]) {
        NSLog(@"Try to add connected receiver: %@", NSStringFromClass([receiver class]));
        return;
    }
    
    [self.receivers addObject:receiver];
    
    /**
     *  workaround: decrease retain count after add to array
     */
    [receiver release];
}

- (void)unsubscribe:(NSObject *)receiver
{
    /**
     *  workaround: increase retain count before remove from array
     */
    [receiver retain];
    
    [self.receivers removeObject:receiver];
}

@end