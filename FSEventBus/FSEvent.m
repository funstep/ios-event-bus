#import "FSEvent.h"
#include "FSEventBus.h"

@implementation FSEvent

- (void)fire
{
    [[FSEventBus instance] fire:self];
}

@end

